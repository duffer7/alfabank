package search;

import java.io.IOException;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Tests extends WebDriverSettings {

    @Test
    public void alfaJobTest() throws IOException {

        driver.get("https://duckduckgo.com");
        String title = driver.getTitle();
        Assert.assertEquals(title, "DuckDuckGo — Privacy, simplified.");

        List<WebElement> elements = driver.findElements(By.tagName("input"));
        WebElement element = elements.get(0);
        element.sendKeys("сайт альфа банка");
        element.submit();
        element = driver.findElement(By.linkText("Альфа-банк"));
        element.click();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        Assert.assertEquals(driver.getTitle(), "Альфа-Банк - кредитные и дебетовые карты, кредиты наличными, автокредитование, ипотека и другие банковские услуги физическим и юридическим лицам – Альфа-Банк");

        element = driver.findElement(By.xpath("//a[@title='Вакансии']"));
        element.click();
        Assert.assertEquals(driver.getTitle(), "Поиск работы, стажировка для студентов - вакансии Альфа-Банк, г. Москва");

        element = driver.findElement(By.xpath("//*[text() = 'О работе в банке']"));
        element.click();
        Assert.assertEquals(driver.getTitle(), "О работе в Альфа-Банк , г. Москва");

        String cur_dir = System.getProperty("user.dir");
        Path path = Paths.get( cur_dir + "/output");
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Assert.assertTrue(Files.isDirectory(path));

        Date objDate = new Date();
        String dateFormat = "ddMMyyyy_kkmmss";
        SimpleDateFormat objSDF = new SimpleDateFormat(dateFormat);
        String date = objSDF.format(objDate);

        Capabilities cap = driver.getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        String[] parts = title.split(" ");
        String se_name = parts[0].toLowerCase();
        String name = cur_dir + "/output/" + date + "_" + browserName + "_" + se_name + ".txt";

        Path file = Paths.get(name);
        if (!Files.exists(file)) {
            try {
                Files.createFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Assert.assertTrue(Files.exists(file));

        element = driver.findElementByClassName("message");
        String content = element.getText() + "\n";
        element = driver.findElementByClassName("info");
        content += element.getText();

        writeToFile(file, content);
        Assert.assertTrue(Files.isReadable(file) && Files.size(file) > 0);
    }

    private void writeToFile(Path file, String content) throws IOException {
        if (content != null && file.isAbsolute()) { Files.write(file, content.getBytes()); }
    }
}
