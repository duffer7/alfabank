import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Parser {

    private static String filename = "values.txt";
    private static File file;
    private static String cur_dir = System.getProperty("user.dir");
    private static String line = "";
    private static BufferedReader buffer;
    private static StringBuffer result = new StringBuffer();
    private static int[] desc_numbers;
    private static String[] string_arr;

    public static void main(String[] args) {
        try {
            if (readFromFile() != null) {

                string_arr = result.toString().split(",");

                int[] asc_numbers = new int[20];
                for (int i = 0; i < string_arr.length - 1; i++) {
                    asc_numbers[i] = Integer.parseInt(string_arr[i]);
                }
                Arrays.sort(asc_numbers);
                desc_numbers = asc_numbers.clone();

                reverseArray(desc_numbers);

                printArray(asc_numbers, "По возрастанию");
                printArray(desc_numbers, "По убыванию");
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private static int[] reverseArray(int[] array) {
        int arrLength = array.length;
        for(int i = 0; i < arrLength / 2; i++) {
            int temp = array[i];
            array[i] = array[arrLength - 1 - i];
            array[arrLength - 1 - i] = temp;
        }
        return array;
    }

    private static void printArray(int[] array, String label) {
        System.out.println(label);
        for(int anArray : array) {
            System.out.println(anArray);
        }
    }

    private static StringBuffer readFromFile() throws IOException {
        file = new File(cur_dir + "/" + filename);
        if(!file.exists()) {
            System.out.println("File not found");
            return null;
        }
        buffer = new BufferedReader(new FileReader(file));
        while ((line = buffer.readLine())!= null) {
            result.append(line);
        }
        return result;
    }
}
