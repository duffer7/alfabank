package ru.yandex;

import junit.framework.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class Tests extends WebDriverSettings {

    private static String websiteURL = "https://yandex.ru";
    private static String yandex_title = "Яндекс";
    private static String market_title = "Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов";
    private static String electronics_title = "Электроника — купить на Яндекс.Маркете";
    private static String market_link_text = "Маркет";
    private static String value_till;
    private static String value_from;
    private static String title;
    private static WebElement element;
    private static String electronics_xpath = "//a[text()='Электроника']";
    private static String headphones_link_text = "Наушники и Bluetooth-гарнитуры";
    private static String mobile_link_text = "Мобильные телефоны";
    private static String headphones_manufacturer = "Производитель Beats";
    private static String mobile_manufacturer = "Производитель Samsung";
    private static String first_element_name;

    @Test
    public void samsungTest() throws InterruptedException {

        enterWebsite(websiteURL);
        Assert.assertEquals(title, yandex_title);

        findElemByLinkText(market_link_text);
        Assert.assertEquals(driver.getTitle(), market_title);

        findElemByXPath(electronics_xpath);
        Assert.assertEquals(driver.getTitle(), electronics_title);

        findElemByLinkText(mobile_link_text);
        Assert.assertEquals(driver.getTitle(), "Мобильные телефоны — купить на Яндекс.Маркете");

        findClickableElementByName(mobile_manufacturer);
        Assert.assertTrue(element.isSelected());

        priceFrom(element, "40000");
        Assert.assertEquals("40000", value_from);

        reloadPage(driver.getCurrentUrl());
        findFirstElement();
        Assert.assertEquals("Смартфон Samsung Galaxy S8+ 64GB", first_element_name);

        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(), first_element_name);

    }

    @Test
    public void beatsTest() throws InterruptedException {

        enterWebsite(websiteURL);
        Assert.assertEquals(title, yandex_title);

        findElemByLinkText(market_link_text);
        Assert.assertEquals(driver.getTitle(), market_title);

        findElemByXPath(electronics_xpath);
        Assert.assertEquals(driver.getTitle(), electronics_title);

        findElemByLinkText(headphones_link_text);
        Assert.assertEquals(driver.getTitle(), "Наушники и Bluetooth-гарнитуры — купить на Яндекс.Маркете");

        findClickableElementByName(headphones_manufacturer);
        Assert.assertTrue(element.isSelected());

        priceFrom(element, "17000");
        priceTill(element, "25000");
        Assert.assertEquals("17000", value_from);
        Assert.assertEquals("25000", value_till);

        reloadPage(driver.getCurrentUrl());
        findFirstElement();
        Assert.assertEquals("Наушники Beats Solo3 Wireless", first_element_name);

        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(), first_element_name);

    }

    private String priceFrom(WebElement element, String price) {
        element = driver.findElement(By.xpath("//input[@name='Цена от']"));
        element.sendKeys(price);
        value_from = element.getAttribute("value");
        return value_from;
    }

    private String priceTill(WebElement element, String price) {
        element = driver.findElement(By.xpath("//input[@name='Цена до']"));
        element.sendKeys(price);
        value_till = element.getAttribute("value");
        return value_till;
    }

    private String enterWebsite(String websiteURL) {
        driver.get(websiteURL);
        title = driver.getTitle();
        return title;
    }

    private void findElemByLinkText(String link_text) {
        element = driver.findElement(By.linkText(link_text));
        element.click();
    }

    private void findElemByXPath(String xpath) {
        element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    private void findClickableElementByName(String name) {
        element = driver.findElement(By.name(name));
        String element_id = element.getAttribute("id");
        WebElement clickable_element = driver.findElement(By.xpath("//label[@for='" + element_id + "']"));
        clickable_element.click();
    }

    private String findFirstElement() {
        List<WebElement> elements = driver.findElements(By.className("n-snippet-cell2"));
        WebElement first_element = elements.get(0);
        first_element = driver.findElement(By.xpath("//*[@data-id='" + first_element.getAttribute("data-id") + "']//following::a"));
        first_element_name = first_element.getAttribute("title");
        first_element.click();
        return first_element_name;
    }

    private void reloadPage(String cur_curl) { driver.get(cur_curl); }
}
